<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211020155406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'fill report data ';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO public.report(export_name, export_time, user_name, place_name)	VALUES ('export 1', now(), 'user2', 'teatr');");
        $this->addSql("INSERT INTO public.report(export_name, export_time, user_name, place_name)	VALUES ('export 2', now(), 'user1', 'szkola');");
        $this->addSql("INSERT INTO public.report(export_name, export_time, user_name, place_name)	VALUES ('export 3', now(), 'user3', 'boisko');");
        $this->addSql("INSERT INTO public.report(export_name, export_time, user_name, place_name)	VALUES ('export 4', now(), 'user4', 'lokalizacja');");


    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
