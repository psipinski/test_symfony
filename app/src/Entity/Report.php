<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $export_name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $export_time;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $place_name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExportName(): ?string
    {
        return $this->export_name;
    }

    public function setExportName(string $export_name): self
    {
        $this->export_name = $export_name;

        return $this;
    }

    public function getExportTime(): ?\DateTimeInterface
    {
        return $this->export_time;
    }

    public function setExportTime(\DateTimeInterface $export_time): self
    {
        $this->export_time = $export_time;

        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->user_name;
    }

    public function setUserName(string $user_name): self
    {
        $this->user_name = $user_name;

        return $this;
    }

    public function getPlaceName(): ?string
    {
        return $this->place_name;
    }

    public function setPlaceName(?string $place_name): self
    {
        $this->place_name = $place_name;

        return $this;
    }
}
