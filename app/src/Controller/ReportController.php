<?php

namespace App\Controller;

use App\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


class ReportController extends AbstractController
{
    /**
     * @Route("/report", name="report")
     */
    public function index(Request $request): Response
    {
        $reportRepository = $this->getDoctrine()->getRepository('App:Report');
        $allReports = $reportRepository->findAll();

        $form = $this->createFormBuilder()
            ->add('place_name', TextType::class, ['label' => 'Lokal'])
            ->add('export_time', DateType::class, ['label' => 'data'])
            ->add('save', SubmitType::class, ['label' => 'Zatwierdź'])
            ->getForm();


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filteredReports = $reportRepository->findBy(array('place_name' => $form->get('place_name')->getViewData()));

            return $this->render('report/index.html.twig', array(
                'report_data' => $filteredReports,
                'form' => $form->createView()
            ));
        }


        return $this->render('report/index.html.twig', [
            'controller_name' => 'ReportController',
            'report_data' => $allReports,
            'form' => $form->createView()
        ]);
    }
}
